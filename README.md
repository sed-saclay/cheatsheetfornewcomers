# Table of contents

- [Table of contents](#table-of-contents)
- [Center departments](#center-departments)
  - [Experimentation and Development Department (SED)](#experimentation-and-development-department-sed)
  - [Information Systems Department (DSI)](#information-systems-department-dsi)
    - [Wifi](#wifi)
    - [VPN](#vpn)
    - [Helpdesk](#helpdesk)
    - [Mail](#mail)
    - [Mailing lists](#mailing-lists)
    - [Visioconference](#visioconference)
    - [Collective work](#collective-work)
  - [Other departments](#other-departments)
- [Developers networks](#developers-networks)
  - [dev-saclay](#dev-saclay)
  - [DevLog](#devlog)
  - [LoOPS](#loops)
  - [Groupe calcul](#groupe-calcul)
- [Tools](#tools)
  - [Gitlab](#gitlab)
  - [Continuous integration](#continuous-integration)
  - [Sonarqube](#sonarqube)
  - [Overleaf](#overleaf)
  - [Mattermost](#mattermost)
    - [Devel group](#devel-group)
    - [Inria](#inria)
    - [SIF](#sif)
    - [Recherche.SIF](#recherchesif)
  - [Clusters](#clusters)
    - [Center clusters](#center-clusters)
    - [Jean Zay](#jean-zay)
- [Miscellaneous](#miscellaneous)
  - [Scientific outreach](#scientific-outreach)
  - [Local Health, Safety and Working Conditions Committee (CLHSCT)](#local-health-safety-and-working-conditions-committee-clhsct)

# Center departments 

## Experimentation and Development Department (SED)

SED gets three main missions:

- Co-development with team-projects: most SED engineers are assigned on development projects within team-projects most of their time.
- Conception, development and maintainance of experimental platforms. Currently there are two such platforms at Inria Saclay:
    - Around 300 nodes of [FIT/IoTLab](https://www.iot-lab.info). SED engineer in charge is Alexandre Abadie.
    - Two wallscreens ([WILD](http://digiscope.fr/en/platforms/wild) and [WILDER](http://digiscope.fr/en/platforms/wilder)) and augmented reality headsets. SED engineer in charge is Olivier Gladin.
- Animation of the community of developers within the center. Concretely, this means you should feel free to **[contact us](mailto:sed-saclay@inria.fr)** if you have any questions or needs regarding software development.

The [SED website](http://sed.saclay.inria.fr) (in french).

## Information Systems Department (DSI)

DSI is responsible for installation and through-life support of digital services and computer tools for researchers and support staff.

DSI maintains a [user documentation](https://doc-si.inria.fr), which was overhauled in 2021; some pages are not yet available in english.

The following [link](https://doc-si.inria.fr/display/SU/Nouvel+arrivant+a+Inria#) [^1] describes many useful SI informations for newcomers (and others...).

I would also like to point out few informations you will need soon enough...

### Wifi 

Two wifi networks you should set up:

- [Eduroam](https://doc-si.inria.fr/display/SU/reseau+partenaire+eduroam), which is a wifi network set up in many academic institutions throughout the world.
- The [Inria interne](https://doc-si.inria.fr/display/SU/Reseau+sans+fil+INRIA-interne) network

### VPN

Many Inria sites are on internal network, and can't be accessed outside of Inria buildings without a VPN (including the SI documentation mentioned above).

The instructions to set up the VPN are [here](https://doc-si.inria.fr/display/SU/VPN) [^1]

Unfortunately the documentation to set it up requires either VPN or internal network; the workaround to consult it is to:

- Log yourself to vpn.inria.fr by using your LDAP credentials 
- Choose "SI - Documentation service VPN"[^1].
- Install the VPN client to be able to use all tools (vpn.inria.fr provides only a subset of the applications on the internal network.)

[^1]: Make sure to select "french" as language, as of July 2023.



### Helpdesk

If you need to warn something isn't working or request assistance, a [Helpdesk site](https://helpdesk.inria.fr/) is there for all SI (and also buildings-related demands). I advise you to use the search bar to find the most adequate category for your demad.

### Mail

If you need to connect an app to your zimbra mail account :

IN
- Protocol: IMAP
- Server name: imap.inria.fr - Port: 993
- User name: main email or login (depends on mail client)
- Security: SSL/TLS

OUT
- Server name: smtp.inria.fr - Port: 587 (for Outlook on Android, it's 465 => smtp.inria.fr:465)
- Security: STARTTLS or TLS

### Mailing lists

If you need to create a mailing list, the service is [Sympa](https://sympa.inria.fr).

### Visioconference

Visioconference tools are listed [here](https://doc-si.inria.fr/display/SU/outils_visio).

### Collective work

- In 2021, an [instance of Codimd](https://notes.inria.fr/) was made available to Inria agents. It is a very useful tool to edit collectively a file in Markdown format (if you're not familiar with it you may use [these notes](https://gitlab.inria.fr/formations/markdown/Markdown) to get the gist of it).

- Other collaborative tools are listed [here](https://doc-si.inria.fr/display/SU/Outils+pour+partager+des+donnees).


## Other departments

There are other departments in the center for other topics (human ressources, budget, innovation and so forth...); when you have a need not covered by DSI or SED ask the assistant (AER) if you're in a team or the head of the SED if you're a SED engineer.



# Developers networks

## dev-saclay

At the level of the center, there is a [mailing list](https://sympa.inria.fr/sympa/info/dev-saclay) onto which news are sent around once a week. New scientific staff (researchers and engineers) are added each month or so and may unsubscribe. If you're not on it and want to be, feel free to contact [Sébastien Gilles](https://annuaire.inria.fr/search?pattern=GILLES%20Sebastien&exact=false) to be added on it.

## DevLog

[DevLog](http://devlog.cnrs.fr/) is a network of mostly academic developers managed by CNRS. Its main asset for Inria developers is a [mailing list](http://devlog.cnrs.fr/identity/register) which is used both for technical exchanges and for sharing job offers.

## LoOPS

[LoOPS](https://reseau-loops.github.io/) is a local network affiliated to DevLog, in which Inria Saclay is implicated through Vincent Rouvreau and Hande Gözükan (SED engineers), who are part of the Comité de Pilotage of the network.

LoOPS organizes [monthly seminars](https://reseau-loops.github.io/cafes/), usually the first Tuesday of the month at 1 pm.

There is also a dedicated [mailing list](https://groupes.renater.fr/sympa/info/loops/).

## Groupe calcul

[Groupe calcul from CNRS](https://calcul.math.cnrs.fr/) also organizes regular seminars and formations.

There is also a mailing list to which you may subscribe on their website using your Inria account.


# Tools

## Gitlab

There is a [dedicated Gitab instance at Inria](https://gitlab.inria.fr/). It is possible to invite non-Inria members to a project through [this portal](https://external-account.inria.fr/).

There are dedicated Inria documentation available at the top of the [help page](https://gitlab.inria.fr/help), which specifies for instance which services are available.

A good practice for Gitlab: your personal space in Gitlab is **not** the best place to store the project you're working on: you should instead put them in the group in which your team stores its project (or if none ask SED for help here). Your own space should be used for pet projects or for forks if your workflow uses them, but it is not meant to be the main location of a code.


## Continuous integration

There is also a [continuous integration facility](https://ci.inria.fr) available at Inria, with its dedicated [documentation](https://ci.inria.fr/doc/). Basically there are three components:

- A [CloudStack farm](https://ci.inria.fr/doc/page/cloudstack_tutorial/) onto which you may create virtual machines to hold your jobs. Options for Linux, Windows and macOS are available.
- A facility to deploy [Jenkins instances](https://ci.inria.fr/doc/page/jenkins_tutorial/).
- Gitlab-CI is also available (and is much easier to set up than Jenkins). There are shared runners that are available; you need to use one of the specific tag in your gitlab-ci.yml (`ci.inria.fr`, `linux` or one specifying the size of the required instance ) to use it. More details [here](https://ci.inria.fr/doc/page/gitlab/#using-shared-runners-linux-only).

## Sonarqube

There is an [Inria instance](https://sonarqube.inria.fr/) of SonarQube, with is a useful platform for code quality. There is an [extensive documentation](https://sonarqube.inria.fr/pages/documentation.html) to see how to use it, with many suggestions for code quality tools.

## Overleaf 

An instance is available [here](http://overleaf.irisa.fr/); you need your Inria Gitlab credentials to log in.

## Mattermost

### Devel group

There is a public group named [Devel](https://mattermost.inria.fr/signup_user_complete/?id=moxgsp3ufjredg6n5a7doeb4eo) for all those interested in software development.

There are many public channels there dedicated to a specific thematic, you may for instance find a channel for [AI community](https://mattermost.inria.fr/devel/channels/ai-community), another for [best practices](https://mattermost.inria.fr/devel/channels/best-practices) and many others.

If there isn't one for your favorite language, feel free to create one!

### Inria

A [group](https://mattermost.inria.fr/signup_user_complete/?id=5ky84c3tq7nbddjjwmz6qf8zqh) for all Inria members; avoid creating new channels here unless it is really something aimed to everyone at Inria (if not it is better to create your own group, typically for your team-project or your department).

### SIF

Same as the previous one for the Inria Saclay center. [This group](https://mattermost.inria.fr/signup_user_complete/?id=q65d85znetg7jnjygp7hw3mqqr) is useful to get informations related to events within the center (seminars, [Unithé ou café](https://intranet.inria.fr/content/view/full/48112), etc...)

### Recherche.SIF

Another [Inria Saclay group](https://mattermost.inria.fr/signup_user_complete/?id=qi78mddfk3b57g8ggcjbk36qge), aimed mostly at researchers.


## Clusters

### Center clusters

For Inria Saclay center, clusters are managed solely by the DSI. Informations may be found on the dedicated [documentation page](https://doc-si.inria.fr/display/SU/Moyens+de+calcul#tab-Saclay).

### Jean Zay

It is possible to request access to Jean Zay cluster, with a simplified procedure for AI projects.

A [documentation](https://github.com/jean-zay-users/jean-zay-doc) is maintained by Inria engineers to help newcomers to use this cluster.

[IDRIS](http://www.idris.fr/eng/index.html) laboratory provides an [advanced support program]( http://www.idris.fr/eng/support_avance-eng.html) in which one of their enginneer may help for few months to use your HPC or AI project on Jean Zay cluster.


# Miscellaneous

## Scientific outreach

If you're willing to participate to scientific outreach, please contact [Irène Vignon-Clémentel](https://annuaire.inria.fr/vigno00E/show) and/or [Mathieu Hemery](https://annuaire.inria.fr/0000G9DG/show) who are in charge of the mediation team.

You are also welcome to join the dedicated [Mattermost channel](https://mattermost.inria.fr/sif/channels/mediation).

## Local Health, Safety and Working Conditions Committee (FSS)

If you're concerned with a issue that is related to the local health, safety or working conditions, you may contact the staff representatives at representants-fss-saclay@inria.fr.
